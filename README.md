# Role of Feature Engineering in Machine Learning

## Introduction

What are features? Feature can be descried as a characteristic or a set of caracteristics. These characteristics can be presented in a measurable form. When the do so, they can be called features.
When we now what features are we can procceed to feature engineering. Ffeature engineering is a process of creating new features out of already existing features in a dataset.
Overall performance of our predictive model depends heavily on quality of the features in datase which is used to train thath model. The general idea is to create enough new features which help in providing more information.

We are going to use Featuretools for the task. It is an open source library for performing automated feature engineering. It is maninly used for the feature generation process, giving user more time to focus on other aspects of machine learning model building.

Featuretools uses three major compontents for automated feature engineering process:

- Entities - An Entity can be considered as a representation of a Pandas DataFrame. A collection of multiple entities is called an Entityset.
- Entitysets - collection of tables and the relationships between entities. It is just another Python data structure, with its own methods and attributes.
- Deep Feature Synthesis (DFS) - stacks multiple transformation and aggregation operations (which are 	called feature primitives in the vocab of featuretools) to create features from data spread across many tables. The depth of a deep feature is the number of primitives required to make the feature.
- Feature primitives - are the building blocks of Featuretools. They define individual computations 	that can be applied to raw datasets to create new features. Because a primitive only constrains the input and output data types, they can be applied across datasets and can stack to create new calculations.
- Transformation - creating new features from the table columns
- Aggegation - creating new features from realted dataset and adding them to already existing dataset. 

## Task: Determine efectiveness of feature engineering


To determine efectiveness of feature engineering we will try tu use featuretools library on two projects. On first project we will also determine a manual features approach.

Project number 1:
-  Predict engine failure. It can be done by trying to predict engine operational cycles. These cycles are tend to fail the longer machine is operating. We can predict the number of cycles since the specified time moment to the moment of failure of the machine(which is the last cycle during the angine will work.

Project number 2:

-  Titanic survival rate using feature engineering

